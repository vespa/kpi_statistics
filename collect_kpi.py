#!/usr/bin/env python3
# -*- coding: UTF8 -*-
#
# The script parses statistic pages from AWSTAT, and dumps the output in JSON
# V0.0 (python2.7) PLS 2020-04-20
# V1.0 (python3.7) BC 2020-04-21

# Status of missing services:
# http://amda-epntap.irap.omp.eu:8080/ soon available
# http://vespa-ae.oma.be: waiting for answer
# http://vogate.obs-nancay.fr: not configured
# http://spectrum.iaa.es:8080/ not available


from urllib.error import URLError
from urllib.request import urlopen

import bs4
import json
import csv
import argparse
from collections import OrderedDict

all_services = {}
with open('services.csv', 'r') as fc:
    title_line = next(fc)
    header_line = next(fc)
    reader = csv.reader(fc.readlines())
    for row in reader:
        all_services[row[0]] = {
            'name': row[1],
            'city': row[2],
            'country': row[3]
        }

all_years = ["2016", "2017", "2018", "2019", "2020"]
months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
all_kpi = ["Unique visitors", "Number of visits", "Number of pages", "Hits", "Data Volume (MB)"]

convert = {'KB': 1/1024, 'MB': 1, 'GB': 1024, 'TB': 1024*1024}


def collect_awstats_data(query_year=None, query_service=None, query_kpi=None):
    """
    Collects data from AWStats access points

    :param query_year: Year of the report (default: None and produces report for all years)
    :type query_year: str, None
    :param query_service: URL of the reported service (default: None and produces report for all services)
    :type query_service: str, None
    :param query_kpi: KPI reported (default: None and produces report for all KPIs)
    :type query_kpi: str, None
    :return: report data
    :rtype: dict
    """

    # processing input keyword `query_year`
    if query_year is None:
        years = all_years
    elif query_year in all_years:
        years = [query_year]
    else:
        raise ValueError("Incorrect year value.")

    # processing input keyword `query_service`
    if query_service is None:
        services = all_services
    elif query_service in all_services.keys():
        services = {query_service: all_services[query_service]}
    else:
        services = {query_service: {"name": "Custom Service"}}

    # processing input keyword `query_title`
    if query_kpi is None:
        kpis = all_kpi
    elif 0 < int(query_kpi) <= len(all_kpi):
        kpis = [all_kpi[int(query_kpi)-1]]
    else:
        raise ValueError("Incorrect parameter value.")

    # doing the job, looping on years first
    # Note: data is an OrderedDict, so that iterating keeps the input order
    data = OrderedDict()
    for year in years:

        # preparing dict structure with selected titles and services
        data[year] = OrderedDict()
        for kpi in kpis:
            # Note: OrderedDict, for the same reasons as above
            data[year][kpi] = OrderedDict()
            for _, service in services.items():
                data[year][kpi][service['name']] = []

        # collecting data from URL and filling in the data dictionary
        for service_url, service_data in services.items():
            awstat_url = f'{service_url}/cgi-bin/awstats.pl?month=01&year={year}' \
                         f'&output=main&config=dachs&framename=mainright'
            try:
                print(f'Parsing: {awstat_url}')

                # How we proceed:
                # search for table rows with the first column in the form `Month YEAR`, this is what we look for.
                # use the subsequent columns as input data
                with urlopen(awstat_url) as html:
                    soup = bs4.BeautifulSoup(html, "lxml")
                    rows = soup.find_all('tr')
                    for tr in rows:
                        cols = tr.find_all('td')
                        if len(cols) > 0:
                            for month in months:
                                if cols[0].text.strip() == f'{month} {year}':
                                    for i, kpi in enumerate(kpis):
                                        value = cols[i+1].text.strip().replace(',', '')
                                        if ' ' in value:
                                            # this case is for the last column, which includes data volume units
                                            # we parse and convert to MB
                                            value, unit = value.split(' ')
                                            value = float(value) * convert[unit]
                                        else:
                                            value = float(value)
                                        data[year][kpi][service_data['name']].append(value)
            except URLError as e:
                print('error', e.reason)

    return data


def merge_years(input_data):
    """
    Converts the internal data into a merged year format
    :param input_data: data dictionary as produced by collect_awstats_data()
    :return: merged year format
    """

    data = dict.fromkeys(all_kpi)  # this is an intermediate dict preparing output into C3.js format
    dates = []  # X axis labels (format = "Mon. Year")

    for year, year_data in input_data.items():
        # appending months labels for the current year
        dates.extend([f'{month} {year}' for month in months])

        for kpi, kpi_data in year_data.items():
            if data[kpi] is None:
                data[kpi] = dict.fromkeys(kpi_data.keys())
            for service, service_data in kpi_data.items():
                if data[kpi][service] is None:
                    data[kpi][service] = service_data
                else:
                    data[kpi][service].extend(service_data)

    return data, dates


def convert_c3js(input_data):
    """
    Converts the internal data dictionary into a C3.js usable JSON structure
    :param input_data: data dictionary as produced by collect_awstats_data()
    :return: C3.js compatible data
    """

    data_tmp, dates = merge_years(input_data)

    # reformatting for final C3.js output
    data = {}
    for kpi, kpi_data in data_tmp.items():
        columns = []
        tmp = ['month']
        tmp.extend(dates)
        columns.append(tmp)
        for service, service_data in kpi_data.items():
            tmp = [service]
            tmp.extend(service_data)
            columns.append(tmp)
        data[kpi] = {
            'data': {
                'x': 'month',
                'xFormat': "%b %Y",
                'columns': columns
            },
            'axis': {
                'x': {
                    'type': 'timeseries',
                    'tick': {
                        'format': "%b %Y"
                    }
                },
                "y": {
                    "label": kpi
                }
            }
        }

    return data


def convert_csv(input_data):
    """
    Converts to CSV (not implemented)
    :param input_data: data dictionary as produced by collect_awstats_data()
    :return: CSV string list
    """
    return input_data


def write_data(input_data, file_name='out.json', file_format='json'):
    """
    Writes out data to the specified file name, with the specified format
    :param input_data: data dictionary as produced by collect_awstats_data()
    :param file_name: file name (default: 'out.json')
    :param file_format: file format (default: 'json')
    :return: None
    """

    with open(file_name, 'w') as fw:

        if file_format in ('json', 'C3.js'):
            if file_format == 'C3.js':
                data = convert_c3js(input_data)
            else:
                data = input_data
            json.dump(data, fw)

        elif file_format == 'csv':
            data = convert_csv(input_data)
            fw.write(data)

        else:
            raise ValueError('unknown output format')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("-o", "--output", help="file name for data output (default: 'out.json')", default='out.json')
    parser.add_argument("-f", "--format", help="formatting for data output (default: 'json')", default='json')
    parser.add_argument("-y", "--year", help="Year of the report (default: all years)", default=None)
    parser.add_argument("-u", "--url", help="URL of the AWStat report (default: all known services)", default=None)
    parser.add_argument("-k", "--kpi", help="KPI parameter id (default: all KPI parameters). List of KPI ids: "
                                            "1: Unique visitors, 2: Number of visits, 3: Number of pages,"
                                            "4: Hits, 5: Data Volume (MB)", default=None)

    args = parser.parse_args()

    write_data(collect_awstats_data(query_year=args.year, query_service=args.url, query_kpi=args.kpi),
               file_name=args.output, file_format=args.format)
