# Europlanet-2024-RI/VESPA Key Performance Indicators (KPI) statistics scripts

## KPI Statistics
All VESPA servers should have a companion [AWStats](https://awstats.sourceforge.io) web 
statistics extraction tool. The script parses each server's AWStats web report and 
extracts the relevant KPI.

The KPI id codes and names are (the numeric id shall be used the `-k` option): 
- 1: *Unique visitors*
- 2: *Number of visits*
- 3: *Number of pages*
- 4: *Hits*
- 5: *Data Volume (in MB)*

There are currently 2 data output formats:
- a JSON file corresponding to the internal data dictionary structure.
- a JSON file that can be used for plotting with C3.js.

The list of currently scanned servers are stored in the [services.csv](services.csv) file.

## Help
```
$ python collect_kpi.py -h
usage: collect_kpi.py [-h] [-v] [-o OUTPUT] [-f FORMAT] [-y YEAR] [-u URL]
                      [-k KPI]

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity
  -o OUTPUT, --output OUTPUT
                        file name for data output (default: 'out.json')
  -f FORMAT, --format FORMAT
                        formatting for data output (default: 'json')
  -y YEAR, --year YEAR  Year of the report (default: all years)
  -u URL, --url URL     URL of the AWStat report (default: all known services)
  -k KPI, --kpi KPI     KPI parameter id (default: all KPI parameters). List
                        of KPI ids: 1: Unique visitors, 2: Number of visits,
                        3: Number of pages,4: Hits, 5: Data Volume (MB)
```

## Example

- Get all data into a file named `c3.json`, with a C3.js compatible formatting:
```
$ python collect_kpi.py -o c3.json -f C3.js
```
- Get *Hits* report data in 2019 for all services:
```
$ python collect_kpi.py -k 4 -y 2019
```

### Plans

* CSV output is planned
